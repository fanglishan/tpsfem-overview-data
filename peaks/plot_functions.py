#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2020-06-13 07:59:05
@Last Modified time: 2023-03-16 22:38:18

This class contains error convergence results for adaptively refined
grids of the TPSFEM for the thesis.



"""

# Import libraries
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from sys import exit
from numpy import log
from copy import deepcopy
import numpy as np


def check_stop(x_list, y_list):
	""" Check if the stopping criteria have been triggered """

	return x_list, y_list

	new_x_list = []
	new_y_list = []

	for i in range(0, len(x_list)):
		print(i, len(y_list[i]))
		rmse_list = y_list[i]

		# If the stopping criteria are satsified
		if abs((rmse_list[-2]-rmse_list[-1])/rmse_list[-2]) < 0.1 and \
			abs((rmse_list[-3]-rmse_list[-2])/rmse_list[-3]) < 0.1:
			print("True")
			# Only include convergence results two iterations prior to the termination
			new_x_list.append(x_list[i][:len(x_list[i])-2])
			new_y_list.append(y_list[i][:len(y_list[i])-2])
		else:
			new_x_list.append(x_list[i])
			new_y_list.append(y_list[i])	

	return new_x_list, new_y_list


def separate_list(x_list, y_list):

	rmse_list = y_list

	# If the stopping criteria are satsified
	if abs((rmse_list[-2]-rmse_list[-1])/rmse_list[-2]) < 0.1 and \
		abs((rmse_list[-3]-rmse_list[-2])/rmse_list[-3]) < 0.1:	
		print("a")
		return x_list[:len(x_list)-2], y_list[:len(y_list)-2], \
			   x_list[len(x_list)-2:len(x_list)], y_list[len(x_list)-2:len(x_list)]

	else:
		return x_list, y_list, None, None



def f(x,A,B):
	return A*x+B


def log_transform(value):
	""" Find log(value) """

	# log(0) is undefined
	if value == 0:
		# Return a large negative value
		# can be negative infinity
		return -100000.0
	else:
		# Return log value
		return log(value)


def validate_input_lists(x_list, y_list, label_list, marker_list):
	""" Check if the size of given lists are correct """

	x_len = len(x_list)
	y_len = len(y_list)
	l_len = len(label_list)
	m_len = len(marker_list)

	if x_len != y_len or y_len != l_len:
		print("Inconsistent list sizes")
		print(x_len, y_len, l_len, m_len)
		exit(-1)

	if m_len < x_len:
		print("Not enough markers")
		exit(-1)


def plot_log_lists(x_list, y_list, label_list, x_label, y_label, plot_line=False):
	""" Plot given lists in loglog scale """

	plt.clf()
	fig = plt.figure()

	# Symbols for different datasets
#	marker_list = ["o-", "^-", "*-", "+-", "s-", "h-", "d-", ",-"]
	marker_list = ["ro-", "g^-", "b*-", "y+-", "ms-", "k,-.", "g^-.", "b*-.", "y+-.", "ms-."]

	validate_input_lists(x_list, y_list, label_list, marker_list)


	if plot_line:

		log_x_list = deepcopy(x_list)
		log_y_list = deepcopy(y_list)

		for i in range(0, len(x_list)):
			for j in range(0, len(x_list[i])):
				log_x_list[i][j] = log_transform(x_list[i][j])
				log_y_list[i][j] = log_transform(y_list[i][j])

		for i in range(0, len(x_list)):
			A,B = curve_fit(f, log_x_list[i], log_y_list[i])[0]
			print(A, B)
#			marker_list[i] += (" " + str("%.1f" % A))


	for i in range(0, len(x_list)):
		plt.loglog(x_list[i], y_list[i], marker_list[i], label=label_list[i])

	plt.legend(loc="upper right")
		
	plt.xlabel(x_label)
	plt.ylabel(y_label)

	plt.xlim(20,10000)
#	plt.xlim(3,1400)
#	plt.ylim(0.02, 0.1)

	plt.show()


def plot_semilogy_lists(x_list, y_list, label_list, x_label, y_label, \
					plot_line=False, x_lim=None, y_lim=None, plot_efficiency=False, termination=[]):
	""" Plot given lists in loglog scale """

#	plt.clf()
	fig = plt.figure()

	# Symbols for different datasets
#	marker_list = ["o-", "^-", "*-", "+-", "s-", "h-", "d-", ",-"]
	marker_list = ["ro-", "g^-", "b*-", "y+-", "ms-", "k<-.", "g^-.", "b*-.", "y+-.", "ms-."]

	space_list = ["  ", "", "", "     ", " ", " "]

#	validate_input_lists(x_list, y_list, label_list, marker_list)

	if plot_line:

		log_x_list = deepcopy(x_list)
		log_y_list = deepcopy(y_list)

		for i in range(0, len(x_list)):
			for j in range(0, len(x_list[i])):
				log_x_list[i][j] = log_transform(x_list[i][j])
				log_y_list[i][j] = log_transform(y_list[i][j])

		for i in range(0, len(x_list)):
			A,B = curve_fit(f, log_x_list[i], log_y_list[i])[0]
#			print A, B
			label_list[i] += (space_list[i] + " " + str("%.1f" % abs(A)))
			
#	termination = [False, False, False, True, False]
#	termination = [False, False, False, False, False]
#	termination = [True, True, True, True, True]
	
	if plot_efficiency:
		for i in range(0, len(x_list)):
			"""
			new_y_list = []
			for j in range(0, len(x_list[i])):
				new_y_list.append(x_list[i][j]*y_list[i][j])
			label_list[i] += (" " + str("%.1f" % abs(sum(new_y_list)/len(new_y_list))))
			"""
			if termination[i]:
				label_list[i] += (" " + str("%.1f" % abs(x_list[i][-3]*y_list[i][-3])))
			else:
				label_list[i] += (" " + str("%.1f" % abs(x_list[i][-1]*y_list[i][-1])))

			print(i, abs(x_list[i][-1]*y_list[i][-1]))

	for i in range(0, len(x_list)):
		"""
		new_y_list = []
		for j in range(0, len(x_list[i])):
			new_y_list.append(x_list[i][j]*y_list[i][j])
		plt.semilogy(x_list[i], new_y_list, marker_list[i], label=label_list[i])
		"""
#		plt.loglog(x_list[i], y_list[i], marker_list[i], label=label_list[i])
#		plt.semilogy(x_list[i], y_list[i], marker_list[i], label=label_list[i])
		print(i, len(y_list[i]))
		print(y_list[i])
		if abs((y_list[i][-2]-y_list[i][-1])/y_list[i][-2]) < 0.1 and \
			abs((y_list[i][-3]-y_list[i][-2])/y_list[i][-3]) < 0.1:
				print("True")
#		plt.semilogy(x_list[i], y_list[i], marker_list[i], label=label_list[i])
		"""
		if i == 0:
			print len(x_list[i][:len(x_list[i])-2]), len(x_list[i][len(x_list[i])-3:len(x_list[i])])
			plt.semilogy(x_list[i][:len(x_list[i])-2], y_list[i][:len(y_list[i])-2], marker_list[i], label=label_list[i])

			marker = marker_list[i][0:2] + ":"
			plt.semilogy(x_list[i][len(x_list[i])-3:len(x_list[i])], y_list[i][len(y_list[i])-3:len(y_list[i])], marker)	
		else:
			plt.semilogy(x_list[i], y_list[i], marker_list[i], label=label_list[i])
		"""
#		if (i == 0 and len(y_list[i]) == 10) or (i != 0 and len(y_list[i]) == 9):

		plt.semilogy(x_list[i], y_list[i], marker_list[i], label=label_list[i])
#
#			marker = marker_list[i][0:2] + ":"
#			plt.semilogy(x_list[i][len(x_list[i])-3:len(x_list[i])], y_list[i][len(y_list[i])-3:len(y_list[i])], marker)
		
#		print str(i)+":", x_list[i][-1]*y_list[i][-1]
		
#	plt.legend(loc="upper right")
	plt.legend()
	plt.grid()
	plt.xlabel(x_label)
	plt.ylabel(y_label)
	
	if x_lim is not None:
		plt.xlim(x_lim[0],x_lim[1])
	if y_lim is not None:
		plt.ylim(y_lim[0],y_lim[1])
#	plt.xlim(3,1400)
#	plt.ylim(0.02, 0.1)

	plt.show()